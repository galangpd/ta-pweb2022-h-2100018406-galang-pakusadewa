<!-- Fungsi ini tanpa return value tetapi dengan parameter! -->
<html>
<head>
<title> Fungsi ini tanpa return value tetapi dengan parameter! </title>
<style type="text/css">
    body{
        background:#5F9EA0; 
    }
</style>
</head>
<body>
</body>
</html>

<?php
function perkenalan($nama, $salam="Assalamualaikum"){
    echo $salam.", ";
    echo "Perkenalkan, nama saya ".$nama."<br/>";
    echo "Senang berkenalan dengan anda <br/>";
}
// Pemanggilan fungsi
echo "<hr>";
perkenalan("Galang");
echo "<hr>";
?>