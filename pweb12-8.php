<!-- Fungsi dengan return value dan parameter -->
<html>
<head>
<title> Fungsi dengan return value dan parameter </title>
<style type="text/css">
    body{
        background:#5F9EA0; 
    }
</style>
</head>
<body>
</body>
</html>

<?php
// Fungsi ini dengan return value dengan parameter parameter!
function hitungUmur($thn_lahir, $thn_sekarang){
  $umur = $thn_sekarang - $thn_lahir;
  return $umur;
}

// Pemanggilan fungsi
echo "<hr>";
echo "Umur saya adalah ". hitungUmur(2003, 2022) ." tahun";
echo "<hr>";
?>