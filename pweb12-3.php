<!-- Array dengan sort() dan rsort() -->
<html>
<head>
<title> Array dengan sort() dan rsort() </title>
<style type="text/css">
    body{
        background:#5F9EA0; 
    }
</style>
</head>
<body>
</body>
</html>

<?php
$arrNilai = array("Galang"=>"4.00", "Dwi"=>"3.95", "Arya"=>"3.90", "Hajril"=>"3.85", 
"Salman"=>"3.80", "Fahrizky"=>"3.75", "Dimas"=>"3.70", "Shidiq"=>"3.65");
echo "<b> Array sebelum diurutkan  : <b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

sort($arrNilai);
reset($arrNilai);
echo "<b> Array setelah diurutkan dengan sort : <b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

rsort($arrNilai);
reset($arrNilai);
echo "<b> Array setelah diurutkan dengan rsort : <b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";
?>